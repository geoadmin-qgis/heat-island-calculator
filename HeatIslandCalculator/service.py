# coding: utf-8
import numpy
import rasterio


class HeatIslandCalculatorServiceException(Exception):
    pass


class HeatIslandCalculatorService(object):
    def __init__(self):
        pass

    def rasterize(self, layer, field, output, width, height, raster_type=4):

        """0 — Byte
        1 — Int16
        2 — UInt16
        3 — UInt32
        4 — Int32
        5 — Float32
        6 — Float64
        7 — CInt16
        8 — CInt32
        9 — CFloat32
        10 — CFloat64"""
        import processing
        processing.runalg('gdalogr:rasterize',
                          layer,
                          field,
                          False,
                          0,
                          width,
                          height,
                          raster_type,
                          output)
        return output

    def calculate_radiance_rasterio(self, rasterLayer, shape, output, param_a=0.03705882, param_b=3.2):

        with rasterio.drivers(CPL_DEBUG=True):

            with rasterio.open(rasterLayer, mode="r", dtype=rasterio.int32) as raster:

                band1 = raster.read(1)

                result = numpy.zeros(shape, dtype=rasterio.float64)
                result += (band1 * param_a) + param_b

                kwargs = raster.meta
                kwargs.update(driver='GTiff',
                              dtype=rasterio.float64,
                              count=1,
                              compress='lzw',
                              nodata=0,
                              bigtiff='YES')

            with rasterio.open(output, "w", **kwargs) as destinationFile:
                destinationFile.write_band(1, result.astype(rasterio.float64))

        return output

    def calculate_brightness_temperature_rasterio(self,
                                                rasterLayer,
                                                shape,
                                                output,
                                                param_a=1282.71,
                                                param_b=666.09,
                                                param_c=1,
                                                param_d=273.15):

        """Calculates this using rasterio"""

        with rasterio.drivers(CPL_DEBUG=True):

            with rasterio.open(rasterLayer, "r", dtype=rasterio.float64) as raster:

                band1 = raster.read(1)

                result = numpy.zeros(shape, dtype=rasterio.float64)
                result += (param_a / numpy.log(param_b / band1) + param_c) - param_d

                kwargs = raster.meta
                kwargs.update(driver='GTiff',
                              dtype=rasterio.float64,
                              count=1,
                              compress='lzw',
                              nodata=0,
                              bigtiff='YES')

            with rasterio.open(output, "w", **kwargs) as destinationFile:
                destinationFile.write_band(1, result.astype(rasterio.float64))

        return output

    def calculate_temperature_raster(self,
                                     shape,
                                     brightnessRasterLayer,
                                     skyViewRasterLayer,
                                     aspectRatioRasterLayer,
                                     imperviousFractionRasterLayer,
                                     anthropogenicHeatRasterLayer,
                                     output):

        with rasterio.drivers(CPL_DEBUG=True):

            with rasterio.open(brightnessRasterLayer, "r", dtype=rasterio.float64) as btRaster:

                with rasterio.open(skyViewRasterLayer, "r", dtype=rasterio.float64) as svRaster:

                    with rasterio.open(aspectRatioRasterLayer, "r", dtype=rasterio.float64) as arRaster:

                        with rasterio.open(imperviousFractionRasterLayer, "r", dtype=rasterio.float64) as ipRaster:

                            with rasterio.open(anthropogenicHeatRasterLayer, "r", dtype=rasterio.float64) as ahRaster:

                                btData = btRaster.read(1)
                                svData = svRaster.read(1)
                                arData = arRaster.read(1)
                                ipData = ipRaster.read(1)
                                ahData = ahRaster.read(1)

                                result = numpy.zeros(svData.shape, dtype=rasterio.float64)
                                try:
                                    result += btData + ahData + (btData * svData * arData * ipData)
                                except ValueError:
                                    raise HeatIslandCalculatorServiceException("Rasters do not match size.")

                                kwargs = btRaster.meta
                                kwargs.update(driver='GTiff',
                                              dtype=rasterio.float64,
                                              count=1,
                                              compress='lzw',
                                              nodata=0,
                                              bigtiff='YES')

        with rasterio.open(output, "w", **kwargs) as destinationFile:

            destinationFile.write_band(1, result.astype(rasterio.float64))

        return output