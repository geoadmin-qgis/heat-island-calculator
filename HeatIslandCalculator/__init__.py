# -*- coding: utf-8 -*-
"""
/***************************************************************************
 HeatIslandCalculator
                                 A QGIS plugin
 Heat Island Calculator
                             -------------------
        begin                : 2015-03-27
        copyright            : (C) 2015 by Muqit Zoarder
        email                : mailbox@muqitzoarder.de
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load HeatIslandCalculator class from file HeatIslandCalculator.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .heat_island_calculator import HeatIslandCalculator
    return HeatIslandCalculator(iface)
