# -*- coding: utf-8 -*-
"""
/***************************************************************************
 HeatIslandCalculatorDialog
                                 A QGIS plugin
 Calculates Heat Islands
                             -------------------
        begin                : 2015-03-25
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Muqit Zoarder
        email                : mailbox@muqitzoarder.de
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import *
from HeatIslandCalculator.service import *

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'heat_island_calculator_dialog_base.ui'))


class HeatIslandCalculatorDialog(QtGui.QDialog, FORM_CLASS):
    outputDirectory = None

    # layers
    skyViewLayer = None
    aspectRatioLayer = None
    imperviousFractionLayer = None
    anthropogenicHeatLayer = None

    # fields
    skyViewField = None
    aspectRatioField = None
    imperviousFractionField = None
    anthropogenicHeatField = None

    def __init__(self, parent=None, iface=None):

        """Constructor."""
        super(HeatIslandCalculatorDialog, self).__init__(parent)
        self.setupUi(self)
        self.iface = iface
        self.resetComboBoxes()
        self.hookSignals()
        self.bar = QgsMessageBar()
        self.bar.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.mainVerticalLayout.insertWidget(0, self.bar)

    def resetComboBoxes(self):

        self.skyViewFeatureComboBox.setCurrentIndex(-1)
        self.aspectRatioFeatureComboBox.setCurrentIndex(-1)
        self.imperviousFractionFeatureComboBox.setCurrentIndex(-1)
        self.anthropogenicHeatFeatureComboBox.setCurrentIndex(-1)

    def hookLayerChange(self, comboBox, function):

        if comboBox and function:
            QObject.connect(comboBox, SIGNAL("layerChanged(QgsMapLayer *)"), function)

    def hookFieldChange(self, comboBox, function):

        if comboBox and function:
            QObject.connect(comboBox, SIGNAL("fieldChanged(QString *)"), function)

    def hookSignals(self):

        self.hookLayerChange(self.skyViewFeatureComboBox, self.handleSkyViewLayerChange)
        self.hookLayerChange(self.aspectRatioFeatureComboBox, self.handleAspectRatioLayerChange)
        self.hookLayerChange(self.imperviousFractionFeatureComboBox, self.handleImperviousFractionLayerChange)
        self.hookLayerChange(self.anthropogenicHeatFeatureComboBox, self.handleAnthropogenicHeatLayerChange)

        QObject.connect(self.selectOutputDirectoryPushButton, SIGNAL("clicked()"), self.handleSelectOutputDirectory)
        QObject.connect(self.calculatePushButton, SIGNAL("clicked()"), self.calculate)

    def handleSkyViewLayerChange(self, map_layer):

        self.skyViewFieldComboBox.setLayer(map_layer)
        self.skyViewLayer = map_layer

    def handleAspectRatioLayerChange(self, map_layer):

        self.aspectRatioFieldComboBox.setLayer(map_layer)
        self.aspectRatioLayer = map_layer

    def handleImperviousFractionLayerChange(self, map_layer):

        self.imperviousFractionFieldComboBox.setLayer(map_layer)
        self.imperviousFractionLayer = map_layer

    def handleAnthropogenicHeatLayerChange(self, map_layer):

        self.anthropogenicHeatFieldComboBox.setLayer(map_layer)
        self.anthropogenicHeatLayer = map_layer

    def handleSelectOutputDirectory(self):

        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.Directory)
        dialog.setOptions(QFileDialog.ShowDirsOnly)
        directory = dialog.getExistingDirectory(self, "Select output directory")

        if directory:
            self.outputDirectoryLineEdit.setText(directory)
            self.outputDirectory = directory

    def validate_input(self):

        """Validates all input for this form"""

        if (self.brightnessRasterComboBox.currentLayer() and
                self.skyViewLayer and
                    self.skyViewFieldComboBox.currentIndex() != -1 and
                self.aspectRatioLayer and
                    self.aspectRatioFieldComboBox.currentIndex() != -1 and
                self.imperviousFractionLayer and
                    self.imperviousFractionFieldComboBox.currentIndex() != -1 and
                self.anthropogenicHeatLayer and
                    self.anthropogenicHeatFieldComboBox.currentIndex() != -1 and
                self.outputDirectory):
            return True

        return False

    def configure_raster_pixel_size(self, rasterLayer):

        """Based on a raster, configure it's pixel size"""

        try:

            self.width = rasterLayer.width()
            self.height = rasterLayer.height()

        except:

            self.width = 5
            self.height = 5

    def load_raster_layer(self, path):

        rasterLayer = QgsRasterLayer(path, os.path.basename(path))
        if rasterLayer.isValid():
            return rasterLayer

        return None

    def clean(self, paths):

        if self.eraseIntermediateDataCheckBox.isChecked():

            for path in paths:

                os.remove(path)

                if os.path.exists(path + ".aux.xml"):
                    os.remove(path + ".aux.xml")

    def load_data(self, paths):

        if self.loadDataWhenDoneCheckBox.isChecked():

            for path in paths:

                if os.path.exists(path):

                    fileInfo = QFileInfo(path)
                    layer = QgsRasterLayer(path, fileInfo.fileName())
                    QgsMapLayerRegistry.instance().addMapLayer(layer, True)

    def calculate(self):

        if self.validate_input():

            self.configure_raster_pixel_size(self.brightnessRasterComboBox.currentLayer())

            service = HeatIslandCalculatorService()

            originalRasterPath = self.brightnessRasterComboBox.currentLayer().dataProvider().dataSourceUri()

            svRasterPath = service.rasterize(self.skyViewLayer,
                                             self.skyViewFieldComboBox.currentField(),
                                             os.path.join(self.outputDirectory, "skyview.tif"),
                                             self.width,
                                             self.height,
                                             raster_type=6)

            arRasterPath = service.rasterize(self.aspectRatioLayer,
                                             self.aspectRatioFieldComboBox.currentField(),
                                             os.path.join(self.outputDirectory, "aspectratio.tif"),
                                             self.width,
                                             self.height,
                                             raster_type=6)

            ifRasterPath = service.rasterize(self.imperviousFractionLayer,
                                             self.imperviousFractionFieldComboBox.currentField(),
                                             os.path.join(self.outputDirectory, "imperviousfraction.tif"),
                                             self.width,
                                             self.height,
                                             raster_type=6)

            ahRasterPath = service.rasterize(self.anthropogenicHeatLayer,
                                             self.anthropogenicHeatFieldComboBox.currentField(),
                                             os.path.join(self.outputDirectory, "anthropogenicheat.tif"),
                                             self.width,
                                             self.height,
                                             raster_type=6)


            rdRasterPath = service.calculate_radiance_rasterio(originalRasterPath,
                                                               shape=(self.height, self.width, ),
                                                               output=os.path.join(self.outputDirectory, "radiance.tif"))

            btRasterPath = service.calculate_brightness_temperature_rasterio(rdRasterPath,
                                                                             shape=(self.height, self.width, ),
                                                                             output=os.path.join(self.outputDirectory, "brightness.tif"))
            try:
                tpRasterPath = service.calculate_temperature_raster((self.height, self.width, ),
                                                                         btRasterPath,
                                                                         svRasterPath,
                                                                         arRasterPath,
                                                                         ifRasterPath,
                                                                         ahRasterPath,
                                                                         os.path.join(self.outputDirectory, "temperature.tif"))
            except HeatIslandCalculatorServiceException:
                self.bar.pushMessage("Invalid input", "The rasters provided do not match size. Make sure the vector layers and the input raster match their sizes.")
                return

            if tpRasterPath:
                self.bar.pushMessage("Process finished successfully", "The calculation of the temperature raster is complete.", QgsMessageBar.INFO)

            self.clean([svRasterPath,
                        arRasterPath,
                        ifRasterPath,
                        ahRasterPath,
                        rdRasterPath,
                        btRasterPath])

            self.load_data([svRasterPath,
                             arRasterPath,
                             ifRasterPath,
                             ahRasterPath,
                             rdRasterPath,
                             btRasterPath,
                             tpRasterPath])


        else:
            self.bar.pushMessage("Invalid input", "We have invalid input. All layer and fields required.",
                                 QgsMessageBar.WARNING)